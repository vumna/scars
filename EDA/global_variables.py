'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ...
Reference: 
'''

Allopurinol = 'Allopurinol'
Carbamazepine = 'Carbamazepine'

tf_models = ['LinearRegression','MLPRegression','MLPClassification','LogisticRegression']
tree_based_tfdf_models = ['RandomForest','Cart','GradientBoosted']
tabnet_models = ['TabnetClassification','TabnetRegression','ElasticNet']
sklearn_models = ['lgbmc','xgboostclassifier']
stats_models = ['statOLS']

label_column_name = 'Phenotype'