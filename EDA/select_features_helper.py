'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ...
'''
import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.model_selection import cross_val_score,RepeatedStratifiedKFold,train_test_split,  GridSearchCV
from sklearn.feature_selection import RFECV, mutual_info_classif, SelectKBest, f_classif, chi2
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error as MSE
from sklearn.datasets import  make_classification, make_regression

from setuptools import setup, find_packages
import umap
import shap
import xgboost
import lightgbm as lgb
import mifs

from optparse import OptionParser
from global_variables import *


fig_width = 40
fig_height = 40
dpi = 300
fontsize = 5


def create_training_opt_parser():
    parser = OptionParser()
    parser.add_option("--data_folder",type=str, 
                        default='../data',
                        help="Input folder path")
    parser.add_option("--prefix_name",type=str,
                        default='SCARS_VN.',
                        help="prefix name")
    parser.add_option("-k","--kind_data",type=str,
                        default=Carbamazepine,
                        help=Allopurinol+'|'+Carbamazepine)
    parser.add_option("--suffix_name",type=str,
                        default='.joint_genotyped.splitted.norm.rename.vcf.recode12.',
                        help="suffix name")
    parser.add_option("--version_run",type=str, 
                        default='v1.0.0test',
                        help="experient th") 
    parser.add_option("--nfeatures_show",type=int,
                        default=50,
                        help="number of features to show")
                        
    parser.add_option("--algorithm",type=str,
                        default='mutual_info_classif',
                        help='Type of algorithm is used for selecting features')

    parser.add_option("--version_out",type=str, 
                        default='v1.0.0test',
                        help="experient th")
    parser.add_option("--output_path",type=str, 
                        default='../../scars_result/',
                        help="output folder path")
    parser.add_option('--temp_folder',type=str,
                            default='temp',
                            help='all temporary files during running will be saved in this folder')
    (options, args) = parser.parse_args()
    return options

def cm_to_inch(value):
    return value/2.54

def mkdir_if_missing(path):
    if not os.path.exists(path):
        os.mkdir(path)


def export_features_rank(options,features_names,values,file_name,algo_name):
    df_temp = pd.DataFrame(list(zip(features_names,values)),
                            columns =['feature', 'score']).sort_values(by='score',ascending=False,ignore_index=True)
    df_temp.to_csv(os.path.join(options.output_path+'/'+algo_name,file_name+'.csv'),index=False)

    #plt.figure(figsize=(cm_to_inch(fig_height),cm_to_inch(fig_width))).set_facecolor("w")
    sns.barplot(y='score', x='feature',data=df_temp.iloc[:options.nfeatures_show,:])
    plt.title(file_name)
    plt.xticks(fontsize=fontsize, rotation=90)
    plt.yticks(fontsize=fontsize)
    plt.savefig(os.path.join(options.output_path+'/'+algo_name,file_name+'.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')
    
#https://cloudstack.ninja/ben-jamin-griff/how-to-use-sklearn-rfecv-to-select-the-optimal-features-to-pass-to-a-dimensionality-reduction-step-before-fitting-my-estimator/
def plot_model_statistics(options,logger,X,file_name,model,algo_name):
    logger.info('plot model statistics')
    export_features_rank(options,list(X.columns),list(max(model.ranking_) - np.array(model.ranking_)),file_name+'_feature_rank',algo_name)
    
    if algo_name=='RFECV':
        file_name = file_name+'_nfeatures_selected'
        min_features_to_select =1
        df_temp = pd.DataFrame.from_dict({'x':list(range(min_features_to_select,len(model.grid_scores_) + min_features_to_select)),
                                            'y':model.grid_scores_})
        df_temp.to_csv(os.path.join(options.output_path+'/'+algo_name,file_name+'.csv'),index=False)

        #plt.figure(figsize=(cm_to_inch(fig_height),cm_to_inch(fig_width))).set_facecolor("w")
        sns.lineplot(x=df_temp['x'],y=df_temp['y'])
        plt.title("Optimal number of features of {} for {}: {}".format(algo_name,file_name,model.n_features_))
        plt.xticks(fontsize=5)
        plt.yticks(fontsize=5)
        plt.xlabel("Number of features selected")
        plt.ylabel("Cross validation score (nb of correct classifications)")
        plt.savefig(os.path.join(options.output_path+'/'+algo_name,file_name+'.png'),bbox_inches='tight',dpi=dpi)
        plt.clf()
        plt.close('all')

def export_model_performance_comparison(options,logger,ex_name,results,model_names,algo_name):
    if len(results)==0:
        logger.info('No result to export in performance comparision')
        return
    file_name = ex_name+'_model_performance_comparison'
    temp_dic = {'model':[],'score':[]}
    for ind,mn in enumerate(model_names):
        for i in results[ind]:
            temp_dic['model'].append(mn)
            temp_dic['score'].append(i)
    df_temp = pd.DataFrame.from_dict(temp_dic)
    df_temp.to_csv(os.path.join(options.output_path+'/'+algo_name,file_name+'.csv'),index=False)

    logger.info('plot model comparison')
    #plt.figure(figsize=(cm_to_inch(fig_height),cm_to_inch(fig_width))).set_facecolor("w")
    sns.boxplot(y='score', x='model',data=df_temp)
    plt.title(ex_name)
    plt.savefig(os.path.join(options.output_path+'/'+algo_name,file_name+'.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')


def get_models_for_RFECV():
    cv = RepeatedStratifiedKFold(n_splits=9, n_repeats=2, random_state=1)
    models = dict()
    #perceptron
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=Perceptron())
    models['per'] = rfe
    # # cart
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=DecisionTreeClassifier())
    models['cart'] = rfe
    # #rf
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=RandomForestClassifier())
    models['rf'] = rfe
    # gbm
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=GradientBoostingClassifier())
    models['gbm'] = rfe
    #svc
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=SVC(kernel='linear'),importance_getter='coef_')
    models['svc'] = rfe
    # abc
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=AdaBoostClassifier())
    models['abc'] = rfe
    # lgbmc
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator= lgb.LGBMClassifier())
    models['lightgbmclassifier'] = rfe
    # xgbc
    rfe = RFECV(cv=cv,n_jobs=-1,scoring='f1_weighted',estimator=xgboost.XGBClassifier())
    models['xgboost']= rfe
    return models


def get_models_for_shap():
    model_dic = {}
    model_dic['cart'] = DecisionTreeClassifier()
    try:
        model_dic['lightgbmclassifier'] = lgb.LGBMClassifier(device='gpu')
        model_dic['xgboost']= xgboost.XGBClassifier(tree_method='gpu_hist', gpu_id=0)
    except:
        model_dic['lightgbmclassifier'] = lgb.LGBMClassifier()
        model_dic['xgboost']= xgboost.XGBClassifier()
    return model_dic