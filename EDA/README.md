## REQUIRED PACKAGES

### ppscore
`pip3 install ppscore`

### plotly
`pip3 install plotly`

### kaleido
`pip3 install kaleido`

### dython
`pip3 install git+https://github.com/mainguyenanhvu/dython.git`

### mifs
`pip3 install git+https://github.com/mainguyenanhvu/mifs.git`

### umap
`pip3 install umap-learn`
