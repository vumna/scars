'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ..
'''
import logging.config
import yaml

with open('./configs/select_features_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')

import os
from tqdm import tqdm
import numpy as np
from numpy import mean, std
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearnex import patch_sklearn
from daal4py.oneapi import sycl_context
patch_sklearn()
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import  GridSearchCV
from sklearn.feature_selection import SelectKBest, f_classif, chi2, mutual_info_classif
from sklearn.metrics import mean_squared_error as MSE
from sklearn.datasets import  make_classification

import ppscore as pps
import umap
import shap
import mifs
from pyGRNN import GRNN
from pyHSICLasso import HSICLasso


from dataset_loader import *
from global_variables import *
from select_features_helper import *

options = create_training_opt_parser()

def RFECV_algo(dataset,extension_name,algo_name='RFECV'):
    logger.info(algo_name)
    extension_name = extension_name+'_'+algo_name
    models = get_models_for_RFECV()
    results, model_names = list(), list()
    fi = open(os.path.join(options.output_path+'/'+algo_name,extension_name+'.txt'),'w')
    for model_name, model in tqdm(models.items()):
        logger.info(model_name)
        try:
            model.fit(dataset.X, dataset.y)
            scores = model.grid_scores_
            results.append(scores)
            model_names.append(model_name)
        except Exception as e:
            logger.error(e)
            continue
        fi.write('>%s %.3f (%.3f)\n' % (model_name, mean(scores), std(scores)))
        plot_model_statistics(options,logger,dataset.X,model_name+'_'+extension_name,model,algo_name)

    fi.close()
    export_model_performance_comparison(options,logger,extension_name,results,model_names,algo_name)

def predictive_power_score(dataset,extension_name,algo_name='pps'):
    logger.info(algo_name)
    file_name = extension_name+'_'+algo_name
    pdt = pps.predictors(dataset.df, label_column_name, sample=None)
    pdt.to_csv(os.path.join(options.output_path+'/'+algo_name,file_name+'.csv'),index=False)
    
    pdt = pdt[['x','ppscore','baseline_score','model_score']].sort_values(by='ppscore',ascending=False,ignore_index=True).iloc[:options.nfeatures_show,:]
    df_temp = pdt.set_index('x').stack().reset_index().rename(columns={'level_1': 'scores', 0: 'value'})
    # sns.barplot(x='x', y='value', data=df_temp, hue='scores',palette="Blues_d")
    sns.barplot(y='x', x='value', data=df_temp, hue='scores',palette="Blues")#sns.color_palette("ch:start=.2,rot=-.3"))
    plt.title(file_name)
    plt.xticks(fontsize=fontsize)#, rotation=90)
    plt.ylabel("feature")
    plt.yticks(fontsize=fontsize)
    plt.legend(loc='upper right')
    plt.savefig(os.path.join(options.output_path+'/'+algo_name,file_name+'.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')

    # for column in pdt.columns:
    #     if 'score' in column and str(pdt[column].dtype)!='bool':
    #         filename = os.path.join(options.output_path, file_name+'_'+column+'.png')
    #         if os.path.isfile(filename):
    #             continue
    #         plt.figure(figsize=(cm_to_inch(fig_height),cm_to_inch(fig_width))).set_facecolor("w")
    #         sns.barplot(y="x", x=column, data=pdt.sort_values(by=column),palette="Blues_d")
    #         plt.title(column + " between each feature and categorical label")
    #         plt.xticks(fontsize=fontsize-15)
    #         plt.yticks(fontsize=fontsize-15)
    #         plt.savefig(filename,bbox_inches='tight',dpi=dpi)
    #         plt.clf()
    #         plt.close('all')

def f_classification(dataset,extension_name,algo_name='f_classif', score_function=f_classif):
    logger.info(algo_name)
    extension_name = extension_name+'_'+algo_name
    fs = SelectKBest(score_func=score_function, k='all')
    fs.fit(dataset.X,dataset.y)
    export_features_rank(options,list(dataset.X.columns),list(fs.scores_),extension_name+'_feature_rank',algo_name)
    
def mutual_info_classification(dataset,extension_name,algo_name='mutual_info_classif', score_function=mutual_info_classif):
    logger
    extension_name = extension_name+'_'+algo_name
    fs = SelectKBest(score_func=score_function, k='all')
    fs.fit(dataset.X,dataset.y)
    export_features_rank(options,list(dataset.X.columns),list(fs.scores_),extension_name+'_feature_rank',algo_name)

def ChiSquared(dataset,extension_name,algo_name='ChiSquared', score_function=chi2):
    logger.info(algo_name)
    logger
    extension_name = extension_name+'_'+algo_name
    fs = SelectKBest(score_func=score_function, k='all')
    fs.fit(dataset.X,dataset.y)
    export_features_rank(options,list(dataset.X.columns),list(fs.scores_),extension_name+'_feature_rank',algo_name)

def umap_algo(dataset,extension_name,algo_name='umap'):
    y_enc = np.array(LabelEncoder().fit(sorted(np.unique(dataset.y))).transform(dataset.y))
    features = {'target': y_enc}
    for name in dataset.X.columns:
        features[name] = dataset.X[name]
    #todo: plot with other
    clusterable_embedding = umap.UMAP(n_neighbors=30,min_dist=0.0,n_components=2,random_state=42,).fit_transform(dataset.X,y_enc)
    for name, value in tqdm(features.items()):
        try:
            plt.scatter(clusterable_embedding[:, 0], clusterable_embedding[:, 1],c=value.astype(int), s=0.1, cmap='Spectral')
            cax = plt.axes([1, 0.1, 0.025, 0.8]) #(left, bottom, width, height)
            plt.colorbar(cax=cax)
            plt.title(extension_name +'_' + name +  " UMAP 2-dimension visualization")
            plt.savefig(os.path.join(options.output_path+'/'+algo_name,extension_name+algo_name+'_'+name+'_2dimen.png'),bbox_inches='tight',dpi=dpi)
            plt.clf()
        except Exception as e:
            logger.error(e)
        plt.close('all')
    clusterable_embedding = umap.UMAP(n_neighbors=30,min_dist=0.0,n_components=3,random_state=42,).fit_transform(dataset.X,y_enc)
    for name, value in tqdm(features.items()):
        try:
            fig = plt.figure()
            ax = Axes3D(fig)
            ax.scatter(clusterable_embedding[:, 0], clusterable_embedding[:, 1], clusterable_embedding[:, 2],c=value.astype(int), s=0.1, cmap='Spectral')
            plt.title(extension_name +'_' + name +  " UMAP 3-dimension visualization")
            plt.savefig(os.path.join(options.output_path+'/'+algo_name,extension_name+algo_name+'_'+name+'_3dimen.png'),bbox_inches='tight',dpi=dpi)
            plt.clf()
        except Exception as e:
            logger.error(e)
        plt.close('all')

def shap_explain(dataset,extension_name, algo_name='shap'):
    logger.info('running SHAP')
    model_dic = get_models_for_shap()
    for model_name, model in tqdm(model_dic.items()):
        logger.info(model_name)
        try:
            model.set_param({"predictor": "gpu_predictor"})
        except Exception as e:
            logger.error(e)
        model.fit(dataset.X,dataset.y)
        logger.info("Fiited!")
        try:
            explainer = shap.Explainer(model)
        except Exception as e:
            logger.error("shap.Explainer {}".format(e))
            continue
        try:
            shap_values = explainer.shap_values(X)
            shap_obj = explainer(X)
        except Exception as e:
            logger.error("explainer {}".format(e))
            continue        
        try:
            plt.figure(figsize=(cm_to_inch(40),cm_to_inch(25))).set_facecolor("w")
            shap.summary_plot(shap_values, X)
            plt.title(extension_name +'_' + model_name +  " SHAP bar")
            plt.savefig(os.path.join(options.output_path+'/'+algo_name,extension_name+algo_name+'_'+model_name+'_bar.png'),bbox_inches='tight',dpi=dpi)
            plt.clf()
        except Exception as e:
            logger.error(e)
        plt.close('all')

def check_selection(selected, i, r):
    """
    Check FN, FP, TP ratios among the selected features.
    """
    # reorder selected features
    try:
        selected = set(selected)
        all_f = set(range(i+r))
        TP = len(selected.intersection(all_f))
        FP = len(selected - all_f)
        FN =  len(all_f - selected)
        if (TP+FN) > 0: 
            sens = TP/float(TP + FN)
        else:
            sens = np.nan
        if (TP+FP) > 0:
            prec =  TP/float(TP + FP)   
        else:
            prec = np.nan
    except:
        sens = np.nan
        prec = np.nan
    return sens, prec
    
def mifs(dataset,extension_name, algo_name='JMI'):
    """
    method: 'JMI' : Joint Mutual Information [1]
            'JMIM' : Joint Mutual Information Maximisation [2]
            'MRMR' : Max-Relevance Min-Redundancy [3]
    """

    s = dataset.df.shape[1]*dataset.df.shape[0]
    f = dataset.df.shape[1]
    i = int(.1*f)
    r = int(.05*f)
    c = 2

    # simulate dataset with discrete class labels in y
    X, y = make_classification(n_samples=s, n_features=f, n_informative=i, 
                        n_redundant=r, n_clusters_per_class=c, 
                        random_state=0, shuffle=False)
    # perform feature selection
    MIFS = mifs.MutualInformationFeatureSelector(method=algo_name, verbose=2)
    MIFS.fit(dataset.X,dataset.y)
    # calculate precision and sensitivity
    sens, prec = check_selection(np.where(MIFS._support_mask)[0], i, r)
    return ('Sensitivity: ' + str(sens) + '    Precision: ' + str(prec))

def pyHSICLasso(path):
    hsic_lasso = HSICLasso()
    hsic_lasso.input(path, output_list=['Clinsig_model'])
    hsic_lasso.classification(10)
    hsic_lasso.dump()
    hsic_lasso.plot_path()
    hsic_lasso.save_param()
    hsic_lasso.save_param()
    return hsic_lasso.get_features()

def pyGRNN(dataset,extension_name, algo_name='pygrnn'):
    # Splitting data into training and testing
    X_train, X_test, y_train, y_test = train_test_split(preprocessing.minmax_scale(X),
                                                        preprocessing.minmax_scale(y.values.reshape((-1, 1))),
                                                        test_size=0.25)

    # Example 1: use Isotropic GRNN with a Grid Search Cross validation to select the optimal bandwidth

    IGRNN = GRNN()
    params_IGRNN = {'kernel':["RBF"],
                    'sigma' : list(np.arange(0.1, 4, 0.01)),
                    'calibration' : ['None']
                    }
    grid_IGRNN = GridSearchCV(estimator=IGRNN,
                            param_grid=params_IGRNN,
                            scoring='neg_mean_squared_error',
                            cv=5,
                            verbose=1
                            )
    grid_IGRNN.fit(X_train, y_train.ravel())
    best_model = grid_IGRNN.best_estimator_
    y_pred = best_model.predict(X_test)
    mse_IGRNN = MSE(y_test, y_pred)

def main():
    mkdir_if_missing(options.output_path)
    options.output_path += options.version_out
    mkdir_if_missing(options.output_path)
    options.temp_folder = options.output_path+'/'+options.temp_folder
    mkdir_if_missing(options.temp_folder)
    mkdir_if_missing(options.output_path+'/'+options.algorithm)

    algo_dict = {'RFECV':RFECV_algo,
                'pps':predictive_power_score,
                'mutual_info_classif': mutual_info_classification,
                'f_classif':f_classification,
                'ChiSquared':ChiSquared,
                'umap': umap_algo,
                'shap':shap_explain}
    # define dataset
    dataset = Dataset(options)
    extension_name = options.kind_data+'_'+options.version_run
    algo_dict[options.algorithm](dataset,extension_name)

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logger.error(e)