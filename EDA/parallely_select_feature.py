'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ...
Reference: https://stackoverflow.com/questions/20821072/multithreading-system-calls-in-python
https://www.geeksforgeeks.org/running-python-script-on-gpu/
'''
#https://stackoverflow.com/questions/51401876/python-logging-all-errors-into-single-log-file
#https://stackoverflow.com/questions/15693529/how-to-specify-in-yaml-to-always-create-log-file-in-the-projects-folder-using-d
import logging.config
import yaml

with open('./configs/paralelly_selected_features_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')

import os
import multiprocessing as mp

from global_variables import *
from select_features_helper import *
options = create_training_opt_parser()

NUM_CPUS = None  # defaults to all available

def start_kfold(file_path='./select_features.py',
                    version_run='v1.0.0',
                    kind_data=Allopurinol,
                    algorithm='mutual_info_classif',
                    data_folder='',
                    version_out='',
                    output_path='',
                    temp_folder='',
                    ):
    os.system('python3 {} --version_run "{}" --kind_data "{}" --algorithm {} --data_folder "{}" --version_out "{}" --output_path "{}" --temp_folder "{}"'.format(
        file_path,version_run,kind_data,algorithm,
        data_folder,version_out,output_path,temp_folder))

def run_pool(pool):
    algorithm = [
        'mutual_info_classif',
        'f_classif',
        'ChiSquared',
        'pps'
    ]
    kind_datas = [Allopurinol,Carbamazepine]
    for algori_name in algorithm:
        for kind_data in kind_datas:
            logger.info('Pooling {} with {} data'.format(algori_name,kind_data))
            options.kind_data = kind_data
            options.algorithm = algori_name
            pool.apply_async(start_kfold,args=(
                    './select_features.py',
                    options.version_run,
                    options.kind_data,
                    options.algorithm,
                    options.data_folder,
                    options.version_out,
                    options.output_path,
                    options.temp_folder
                    ))
            # start_kfold('./select_features.py',
            #         options.version_run,
            #         options.kind_data,
            #         options.algorithm,
            #         options.data_folder,
            #         options.version_out,
            #         options.output_path,
            #         options.temp_folder)

if __name__ == "__main__":
    try:
        with mp.Pool(NUM_CPUS) as pool:
            run_pool(pool)
            pool.close()
            pool.join()
    except Exception as e:
        logger.exception(e)