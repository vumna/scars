'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: Dataset class controls intialization and splitting of data for select features
Reviewed by: ...
Reference: 
'''
import pandas as pd
from global_variables import *

global col_names

class Dataset():
    def __init__(self,opt):
        self.file_path = opt.data_folder + '/' + opt.prefix_name + opt.kind_data + opt.suffix_name+opt.version_run + '.csv'

        self.ndim = 0
        self.shape = None
        self.classnum = None
        self.classname = None
        self.X = self.y = None
        self.input_feature_names = None
        self.load_data()
        

    def load_data(self):
        self.df = pd.read_csv(self.file_path)
        self.X = self.df.drop([label_column_name],axis=1,errors='ignore')
        self.y = self.df[label_column_name]

        self.input_feature_names = self.X.columns
        self.classname = sorted(list(self.df[label_column_name].unique()))
        self.classnum = len(self.classname)
        self.shape = [len(self.input_feature_names)]