'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/08/2021
Last Modified: 11/08/2021
Description: 
Reviewed by: ...
'''
import os
import re
# from numpy cimport ndarray as ar
# cimport cython
import numpy as np
import pandas as pd

csv_file = '/home/vumna/VG1000/SCARS/Data/thyroid_cancer.add_phenotye.csv'
output_folder = '/home/vumna/VG1000/SCARS/Data/'

# @cython.boundscheck(False)
# @cython.wraparound(False)
# def toarr(xy):
#     cdef int i, j, h=len(xy), w=len(xy[0])
#     cdef ar[double,ndim=2] new = np.empty((h,w))
#     for i in xrange(h):
#         for j in xrange(w):
#             new[i,j] = xy[i][j]
#     return new

def read_file():
    print("Reading")
    file_arr = []
    co = 0
    with open(csv_file,'r') as f:
        for line in f:
            line = line.strip().split(',')[:12]
            if int(line[5]) < 0:
                continue
            # temp_el = line[0]
            # line[0] = line[5]
            # line[5] = line[2]
            # line[2] = temp_el
            temp_el = line[2]
            line[2] = line[5]
            line[5]=temp_el
            file_arr.append(line)
            # co+=1
            # if co ==3:
            #     break
    file_arr = list(map(list, zip(*file_arr)))
    vars_arr = np.array(file_arr[6:]).astype(np.int8)
    print(vars_arr.shape)
    #header = ['Family_ID','Individual_ID','Paternal_ID','Maternal_ID','Gender','Phenotype']
    # header = ['Phenotype','Individual_ID','Family_ID','Maternal_ID','Gender','Paternal_ID',]
    header = ['Family_ID','Individual_ID','Phenotype','Maternal_ID','Gender','Paternal_ID']
    for i in range((vars_arr.shape[0])//2):
        header.extend(['rs'+str(i)+'_1','rs'+str(i)+'_2'])
    return file_arr, vars_arr,header

def additive_model(file_arr, vars_arr,header):
    print("Additive")
    temp = (vars_arr[:-1:2]+vars_arr[1::2]).tolist()
    for i in range(len(temp)):
        header.extend(['rs'+str(i)+'_1_additive'])
    file_arr.extend(temp)

def multiply_model(file_arr, vars_arr,header):
    print("Multiplyyyyyy")
    temp = (vars_arr[:-1:2]*vars_arr[1::2]).tolist()
    for i in range(len(temp)):
        header.extend(['rs'+str(i)+'_1_multiply'])
    file_arr.extend(temp)

def write_file(file_arr,header):
    file_arr = list(zip(*file_arr))
    file_np_arr = np.array(file_arr)
    with open(os.path.join(output_folder,'thyroid_cancer.add_model.csv'),'w') as f:
        np.savetxt(f,file_np_arr,fmt='%s',delimiter=',',header=','.join(header))
    # file_arr = list(map(list, zip(*file_arr)))
    # fo = open(os.path.join(output_folder,'thyroid_cancer.add_model.csv'),'w')
    # fo.write(','.join(header))
    # fo.write('\n')
    # for line in file_arr:
    #     fo.write(','.join(line))
    #     fo.write('\n')
    # fo.close()

def main():
    file_arr, vars_arr,header = read_file()
    additive_model(file_arr, vars_arr,header)
    multiply_model(file_arr, vars_arr,header)
    write_file(file_arr,header)

if __name__ == "__main__":
    main()