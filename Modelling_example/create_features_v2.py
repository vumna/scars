'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/08/2021
Last Modified: 11/08/2021
Description: 
Reviewed by: ...
'''
import os
import re
import subprocess
import numpy as np
from optparse import OptionParser


parser = OptionParser()
parser.add_option("--data_folder",type=str, 
                    default='../data',
                    help="Get train test csv files")
parser.add_option("--input_file",type=str,
                    default='thyroid_cancer.full_samples.csv')
parser.add_option("--b0",type=int, 
                    default=3)
parser.add_option("--b1",type=int, 
                    default=4)
parser.add_option("--b2",type=int, 
                    default=5)
parser.add_option("--a_value",type=int, 
                    default=1)
parser.add_option("--A_value",type=int, 
                    default=2)
parser.add_option("--output_folder",type=str, 
                    default='../data',
                    help="output folder path")
parser.add_option("--output_file_name", type=str,
                    default=None)
parser.add_option("--output_version",type=str,
                    default='v1.0.0')
(options, args) = parser.parse_args()

csv_file = os.path.join(options.data_folder, options.input_file)
a_val = options.a_value
A_val = options.A_value

n_models = 2
basic_info_columns = 0

def read_file():
    global basic_info_columns
    print("Reading")
    process = subprocess.check_output("head -1 "  + csv_file +" | sed 's/[^,]//g' | wc -c", shell=True)
    skip_columns = 2
    data_columns = int(re.sub('[^0-9]+', '', str(process)))-skip_columns
    #print(data_columns)
    process = subprocess.Popen(['wc', '-l',csv_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    data_rows = int(re.sub('[^0-9]+', '', str(process.communicate()[0]).split(' ')[0]))
    #header = ['Family_ID','Individual_ID','Paternal_ID','Maternal_ID','Gender','Phenotype']
    #header = ['Family_ID','Individual_ID','Phenotype','Maternal_ID','Gender','Paternal_ID']
    header = ['Phenotype','Maternal_ID','Gender','Paternal_ID']
    basic_info_columns = len(header)
    data_np = np.empty((data_rows,data_columns+(data_columns-basic_info_columns)//2*n_models)).astype(np.int8)

    ind = -1
    with open(csv_file,'r') as f:
        for line in f:
            #line = line.strip().split(',')[skip_columns:12]
            line = line.strip().split(',')[skip_columns:]
            if int(line[3]) < 0:
                continue
            temp_el = line[0]
            line[0] = line[3]
            line[3]=temp_el
            ind+=1
            data_np[ind,:data_columns] = line
    data_np.resize((ind+1,data_columns+(data_columns-basic_info_columns)//2*n_models))
    for i in range((data_columns-basic_info_columns)//2):
        header.extend(['rs'+str(i)+'_1','rs'+str(i)+'_2'])
    return data_np,header, data_columns

def additive_model(data_np,header,data_columns):
    '''
    a/a: a*b0+a*b0
    a/A: a*b0+A*b1
    A/A: A*b1+A*b1
    '''
    print("Additive")
    current_columns = len(header)
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0+data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0+A_val*options.b0: a_val*options.b0+A_val*options.b1,
                    A_val*options.b0+A_val*options.b0: 2*A_val*options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_additive'])

def multiplication_model(data_np,header,data_columns):
    '''
    a/a: a*b0*a*b0
    a/A: a*bo*A*b1
    A/A: A*b1*A*b1
    '''
    print("Multiply")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*A_val*options.b0: a_val*options.b0*A_val*options.b1,
                    A_val*options.b0*A_val*options.b0: (A_val*options.b1)**2}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_multiply'])

def genotypic_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0+b1+b2
    A/A: b0+b1
    '''
    print("genotypic")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1+options.b2,
                    A_val*options.b0*A_val*options.b0:options.b0+options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_genotypic'])

def dominant_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0+b1
    A/A: b0+b1
    '''
    print("Dominant")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1,
                    A_val*options.b0*A_val*options.b0:options.b0+options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_dominant'])

def recessiv_model(data_np,header,data_columns):
    print("Recessiv")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0,
                    A_val*options.b0*A_val*options.b0:options.b0+options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_recessiv'])

def codominant_model(data_np,header,data_columns):
    print("codominant")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0+2*options.b1,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1+options.b2,
                    A_val*options.b0*A_val*options.b0:options.b0+2*options.b2}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    for i in range(added_columns):
        header.extend(['rs'+str(i)+'_1_codominant'])

def write_file(data_np,header):
    print("Outputting")
    if options.output_file_name is None:
        options.output_file_name = options.input_file.replace('csv',options.output_version) + 'csv'
    with open(os.path.join(options.output_folder,options.output_file_name),'w') as f:
        np.savetxt(f,data_np,fmt='%s',delimiter=',',header=','.join(header))
    # data_np = list(map(list, zip(*data_np)))
    # fo = open(os.path.join(options.output_folder,'thyroid_cancer.add_model.csv'),'w')
    # fo.write(','.join(header))
    # fo.write('\n')
    # for line in data_np:
    #     fo.write(','.join(line))
    #     fo.write('\n')
    # fo.close()

def main():
    global n_models
    n_models = 6
    data_np,header, data_columns = read_file()
    additive_model(data_np,header, data_columns)
    multiplication_model(data_np,header, data_columns)
    genotypic_model(data_np,header, data_columns)
    dominant_model(data_np,header, data_columns)
    recessiv_model(data_np,header, data_columns)
    codominant_model(data_np,header, data_columns)
    write_file(data_np,header)

if __name__ == "__main__":
    main()