'''
Author: Anh-Vu Mai-Nguyen
Created Date: 10/09/2021
Last Modified: 16/09/2021
Description: 
Reviewed by: ...
to-do:
    - 
    - Add SHAP to explain model
Bugs: https://github.com/tensorflow/tensorflow/issues/40919 (solution: set batch_size=1)
'''
#https://stackoverflow.com/questions/51401876/python-logging-all-errors-into-single-log-file
#https://stackoverflow.com/questions/15693529/how-to-specify-in-yaml-to-always-create-log-file-in-the-projects-folder-using-d
import logging.config
import yaml

with open('./configs/train_single_model_logging_config.yml', 'r') as config:
    logging.config.dictConfig(yaml.safe_load(config))

logger = logging.getLogger('cloaked_chatter')#getLogger(__name__)

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import warnings
warnings.filterwarnings('ignore')

from contextlib import redirect_stdout
import sys
import pickle
import numpy as np

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint

#import tensorflow_addons as tfa
# config = tf.compat.v1.ConfigProto()
# config.gpu_options.allow_growth = True
# session = tf.compat.v1.Session(config=config)

from sklearnex import patch_sklearn
from daal4py.oneapi import sycl_context
patch_sklearn()

from training_helper import *
from model_admin import *
from dataset_admin import *
from global_variables import *


def prepare_folders():
    mkdir_if_missing(options.output_folder)
    options.output_folder = os.path.join(options.output_folder,'training')
    mkdir_if_missing(options.output_folder)
    options.output_folder = os.path.join(options.output_folder,options.version_run)
    options.history_folder = os.path.join(options.output_folder,options.history_folder)
    options.model_save_folder = os.path.join(options.output_folder,options.model_save_folder)
    options.temp_folder = os.path.join(options.output_folder,options.temp_folder)
    mkdir_if_missing(options.output_folder)
    mkdir_if_missing(options.model_save_folder)
    mkdir_if_missing(options.history_folder)
    mkdir_if_missing(options.temp_folder)

def initialize_variables():
    #extent_name = options.version_run
    options.file_name = options.model_name+'_'+options.kind_data+'_'+options.problem_statement+'_'+options.preprocess_method+options.version_run
    options.model_file = os.path.join(options.model_save_folder,options.file_name+'.h5')
    
    metrics = [
        keras.metrics.KLDivergence(name='kld'),
        #keras.losses.SparseCategoricalCrossentropy(name='sparse_categorical_crossentropy'),
        keras.metrics.SparseCategoricalAccuracy(name='sparse_categorical_accuracy'),
    ]
    checkpoint = ModelCheckpoint(options.model_file, 
                            verbose=options.verbose,
                            monitor='val_loss', 
                            save_best_only=True, 
                            mode='min')
    # options.epoch = 1
    if (options.model_name in tree_based_tfdf_models) or (options.model_name in tabnet_models):
        if options.model_name in tree_based_tfdf_models:
            options.epoch = 1
            options.loss = 'sparse_categorical_accuracy'
            metrics = [
                #keras.losses.SparseCategoricalCrossentropy(name='sparse_categorical_crossentropy'),
                keras.metrics.SparseCategoricalAccuracy(name='sparse_categorical_accuracy'),
                keras.metrics.KLDivergence(name='kld')
            ]
        checkpoint = ModelCheckpoint(options.model_file, 
                                verbose=options.verbose,
                                monitor='val_loss', 
                                save_best_only=True, 
                                mode='min',
                                save_format="h5",
                                save_weights_only=True)
    callbacks_list = [checkpoint]

    if options.problem_statement == REGRESSION_TASK:
        options.loss = 'mse'
        metrics = [#keras.metrics.MeanSquaredError(name="mse"),
        keras.metrics.RootMeanSquaredError(name="rmse"),
        keras.metrics.MeanAbsoluteError(name="mae")
        ]
    return metrics, callbacks_list

def save_model(model,dataset,history):
    if options.model_name in sklearn_models + stats_models: #https://machinelearningmastery.com/save-load-machine-learning-models-python-scikit-learn/
        pickle.dump(model, open(options.model_file, 'wb'))
    
    with open(os.path.join(options.history_folder,options.file_name+'_'+'summary.txt'),'w') as f:
        with redirect_stdout(f):
            if options.model_name in tf_models + tree_based_tfdf_models + tabnet_models:
                model.summary()
            elif options.model_name in stats_models:
                variable_names = ['const']
                print(model.summary(xname=variable_names+dataset.input_feature_names))
    if options.model_name in tree_based_tfdf_models:
        plot_loss_tree(model, options.history_folder,options.file_name)
        plot_model_tree(model, options.history_folder,options.file_name)
        plot_feature_importance_tfdf(model, options.history_folder, options.file_name)
    elif options.model_name in tabnet_models:
        plot_feature_importance_tabnet(model, options.history_folder, options.file_name)
    elif options.model_name in tf_models:
        try:
            draw_model(model, options.history_folder, options.file_name)
        except Exception as e:
            logger.error(e)
    try:
        shap_explain(model,dataset,options.history_folder, options.file_name)
    except Exception as e:
        logger.error(e)

    save_history(history, options.history_folder, options.file_name)
    plot_history(history, options.history_folder, options.file_name)

def evaluate_model(model,dataset):
    if options.model_name in tf_models + tree_based_tfdf_models + tabnet_models:
        test_predictions_baseline = model.predict(dataset.test_dataset)
    elif options.model_name in sklearn_models:
        test_predictions_baseline = model.predict(dataset.X_test)
    elif options.model_name in stats_models: #https://github.com/marcopeix/ISL-linear-regression/blob/master/Linear%20Regression.ipynb
        X2 = sm.add_constant(dataset.X_test)
        test_predictions_baseline = model.predict(X2)
    if options.model_name in tf_models + tree_based_tfdf_models + tabnet_models:
        pred_labels = np.argmax(test_predictions_baseline, axis=1) 
    else:
        pred_labels = test_predictions_baseline
    
    #test_lbs = np.concatenate([y for x, y in dataset.test_dataset], axis=0)
    test_labels = dataset.y_test#test_lbs
    if options.problem_statement == CLASSIFICATION_TASK:
        try:
            plot_cm(options.history_folder,options.file_name,test_labels, pred_labels,dataset)
        except Exception as e:
            logger.error(e)
        try:
            plot_roc(options.history_folder,options.file_name,test_labels, pred_labels, dataset)
        except Exception as e:
            logger.error(e)
    save_scores(options.history_folder,options.file_name,test_labels,pred_labels,options.problem_statement)

def training_pipeline():
    prepare_folders()
    
    logger.info("Loading data")
    dataset = Dataset(options)
    logger.info("Training: training samples {}; testing samples {}".format(str(int(dataset.dataset_nrows*0.7)),str(int(dataset.dataset_nrows-int(dataset.dataset_nrows*0.7)))))
    # logger.info("Training: training samples {}; testing samples {}".format(str(dataset.train_dataset.cardinality().numpy()),str(dataset.test_dataset.cardinality().numpy())))
    # logger.info("Training shape {}; Testing shape: {}".format(np.array(list(dataset.train_dataset.as_numpy_iterator())).shape,
    #                                                             np.array(list(dataset.test_dataset.as_numpy_iterator())).shape))

    if options.model_name in tf_models + tree_based_tfdf_models + tabnet_models:
        dataset.convert_np_to_tf_data(options)
    model,options.model_name = create_model(options.model_name,dataset)
    logger.info("Generated model " + options.model_name)
    
    metrics, callbacks_list  = initialize_variables())
    
    if options.model_name in tree_based_tfdf_models:
        model.compile(metrics=metrics)
    elif options.model_name in tabnet_models:
        model.compile(optimizer=options.optimizer,loss=options.loss,metrics=metrics,run_eagerly=True)
    elif options.model_name in tf_models:
        model.compile(optimizer=options.optimizer,loss=options.loss,metrics=metrics)
    
    logger.info("Training model")
    if options.model_name in tf_models + tree_based_tfdf_models + tabnet_models:
        history = model.fit(dataset.train_dataset,
                # steps_per_epoch=nb_train_samples // options.batch_size,
                validation_data=dataset.test_dataset,
                # validation_steps=nb_validation_samples // options.batch_size,
                epochs=options.epoch, 
                verbose=options.verbose, 
                callbacks=callbacks_list, 
                use_multiprocessing=False)
    else:
        history = None
        if options.model_name in sklearn_models:
            model.fit(dataset.X,dataset.y)
        elif options.model_name in stats_models:
            X2 = sm.add_constant(dataset.X)
            model = model(dataset.y, X2)
            model = model.fit()

    logger.info("Saving model")
    save_model(model,dataset,history)
    
    logger.info("Evaluating")
    evaluate_model(model,dataset)

if __name__ == '__main__':
    try:
        options, args = create_training_opt_parser()
        gpu_id = get_gpu_id_max_memory(options.GPU_memory_used)
        if gpu_id == -1:
            logger.info("Can't run on GPUs now because of lacking available GPU memory!")
        else:
            os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_id)
        training_pipeline()
    except Exception as e:
        logger.exception(e)