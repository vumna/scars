'''
Author: Anh-Vu Mai-Nguyen
Created Date: 05/04/2021
Last Modified: 05/04/2021
Description: 
Reviewed by: ...
Reference: 
'''

### model_admin.py ###
INPUT_SHAPE = None
INPUT_DIM = None
CLASS_NUM = None
MAX_TRIALS = 1

CLASSIFICATION_TASK = 'classification'
REGRESSION_TASK = 'regression'

tf_models = ['LinearRegression','MLPRegression','MLPClassification','LogisticRegression']
tree_based_tfdf_models = ['RandomForest','Cart','GradientBoosted']
tabnet_models = ['TabnetClassification','TabnetRegression','ElasticNet']
sklearn_models = ['lgbmc','xgboostclassifier']
stats_models = ['statOLS']

target_column_name = 'Phenotype'
exclude_cols=['Family_ID','Individual_ID']
drop_columns=['Family_ID','Individual_ID','Phenotype']