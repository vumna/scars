'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/08/2021
Last Modified: 11/08/2021
Description: 
Reviewed by: ...
'''
import os
import re
import numpy as np
import pandas as pd

ped_file = '../data/thyroid_cancer.ped'
info_file = '../data/GSE67047_metadata_2_T.txt'
output_folder = '../data/'

def read_info():
    print("Reading info")
    fi = open(info_file,'r')
    phenotypes = titles = geo_accession = None
    line = fi.readline()
    while True:
        if ('Sample_title' in line):
            titles = re.sub("[^0-9\t]", "",line).split('\t')[1:]
        if ('Sample_geo_accession' in line):
            geo_accession = line.strip().split('\t')[1:]
        if ('Sample_characteristics_ch1' in line) and ('phenotype' in line):
            phenotypes = line.strip().replace('phenotype: ','').split('\t')[1:]
            break
        line = fi.readline()
    fi.close()
    phenotypes = [x.replace('MTC','1').replace('PTC','2').replace('Control','0') for x in phenotypes]
    ids = [x+'-'+y for x,y in zip(geo_accession,titles)]
    phenotypes_dict = dict(zip(ids,phenotypes))
    return phenotypes_dict

def read_ped():
    print("Reading ped")
    ped_arr = []
    with open(ped_file,'r') as f:
        for ind,line in enumerate(f):
            #print(ind)
            ped_arr.append(line.strip().split(' '))
    return ped_arr

def add_phenotype(phenotypes_dict,ped_arr):
    print("Adding phenotype")
    for i in range(len(ped_arr)):
        id = ped_arr[i][0]
        if id in phenotypes_dict.keys():
            ped_arr[i][5] = str(phenotypes_dict[id])
        else:
            ped_arr[i][5] = str(2) #PTC
    return ped_arr

def output(ped_arr):
    # header = ['Family_ID','Individual_ID','Paternal_ID','Maternal_ID','Gender','Phenotype']
    # for i in range((len(ped_arr[1])-6)/2):
    #     header.extend(['rs'+str(i)+'_1','rs'+str(i)+'_2'])
    print("outputing")
    fo = open(os.path.join(output_folder,'thyroid_cancer.full_samples.csv'),'w')
    for line in ped_arr:
        fo.write(','.join(line))
        fo.write('\n')
    fo.close()

def main():
    phenotypes_dict = read_info()
    ped_arr = read_ped()
    ped_arr = add_phenotype(phenotypes_dict,ped_arr)
    output(ped_arr)

if __name__ == "__main__":
    main()