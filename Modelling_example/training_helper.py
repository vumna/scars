'''
Author: Anh-Vu Mai-Nguyen
Created Date: 19/06/2021
Last Modified: 23/07/2021
Description: 
Reviewed by: ...
'''

import os
import subprocess
from optparse import OptionParser

import math
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

import tensorflow as tf
from tensorflow.keras.utils import plot_model
import tensorflow_decision_forests as tfdf

from sklearn.metrics import classification_report, confusion_matrix, balanced_accuracy_score, f1_score, precision_score, recall_score, mean_absolute_error, mean_squared_error 
from sklearn.preprocessing import label_binarize

from dython.model_utils import metric_graph, random_forest_feature_importance
import shap

from global_variables import *

dpi = 300

def create_training_opt_parser():
    parser = OptionParser()
    ################################ Data parameters #####################################
    parser.add_option("-d", "--dataset_path",type=str, 
                        default='../data',
                        help="Dataset folder path to load csv file")
    parser.add_option("--version_run",type=str, 
                        #default='',
                        #default='',
                        default='v1.0.0',
                        help="dataset version used")
    parser.add_option("--preprocess_method",type=str,
                        default='',
                        help='')
    parser.add_option("--kind_data",type=str,
                        default='MTC',
                        help='full|MTC|PTC')
    parser.add_option("--run_preprocess",
                        default=False,
                        help='produce temp files dataset for model (True) or use produced files (False)')

    ############################# Output parameters #########################################
    parser.add_option("-o", "--output_folder",type=str, 
                        default='../../scars_result/result',
                        #default='./resulttemp',
                        help="The folder contains all other output folders")
    parser.add_option('--model_save_folder',type=str,
                        default='model_files',
                        help='model file will be saved in this folder')
    parser.add_option('--history_folder',type=str,
                        default='history',
                        help='training history will be saved in this folder')
    parser.add_option('--temp_folder',type=str,
                        default='temp',
                        help='all temporary files during running will be saved in this folder')
    
    ############################### Model parameters ########################################
    parser.add_option("--problem_statement",type=str,
                        default='classification',
                        help='classification|regression')
    parser.add_option("-n", "--model_name",type=str, 
                        default='LogisticRegression',
                        help="Available model to train")

    ################################### Training parameters ##########################
    parser.add_option("-e","--epoch", type=int, 
                        default=1,
                        help="number of training epoch")
    parser.add_option("-l","--loss", type=str, 
                        default="sparse_categorical_crossentropy",
                        help="training loss function folllowing tf.keras.metrics")
    parser.add_option("--optimizer", type=str, 
                        default='Adam',
                        help="training optimizer following tf.keras.optimizers")
    parser.add_option("--learning_rate", type=float, 
                        default=0.003,
                        help="learning rate")
    parser.add_option("--batch_size", type=int, 
                        default=32,
                        help="training batch size")
    parser.add_option("-v", "--verbose", type=int, 
                        default=2,
                        help="whether or not print status messages to stdout")
    parser.add_option("--GPU_memory_used", type=int, 
                        default=1024,
                        help="how much MB needs running on GPU")
    parser.add_option("--multi_gpu", type=int,
                        default=1,
                        help="how many gpus need running on")

    (options, args) = parser.parse_args()
    return options, args

def cm_to_inch(value):
    return value/2.54

def get_gpu_id_max_memory(acceptable_available_memory):
    _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]
    COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"
    try:
        memory_free_info = _output_to_list(subprocess.check_output(COMMAND.split()))[1:]
    except:
        return -1
    memory_free_values = np.array([int(x.split()[0]) for i, x in enumerate(memory_free_info)])
    print(memory_free_values)
    gpu_id = memory_free_values.argmax()
    if memory_free_values[gpu_id] < acceptable_available_memory:
        return -1
    print(gpu_id)
    return [str(gpu_id)]


def get_gpu_ids_adapt_memory(acceptable_available_memory):
    _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]
    COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"
    try:
        memory_free_info = _output_to_list(subprocess.check_output(COMMAND.split()))[1:]
    except:
        return -1
    memory_free_values = np.array([int(x.split()[0]) for i, x in enumerate(memory_free_info)])
    print(memory_free_values)
    gpu_ids = [str(i) for i in range(len(memory_free_values)) if memory_free_values[i]>=acceptable_available_memory]
    if len(gpu_ids) == 0:
        gpu_ids = -1
    print(gpu_ids)
    return gpu_ids

def mkdir_if_missing(path):
    if not os.path.exists(path):
        os.mkdir(path)

def draw_model(model,his_path,file_name):
    plot_model(model, to_file=os.path.join(his_path,file_name+'_model.png'))

def save_history(history, his_path, file_name):
    if history is None:
        return
    # headers = ['epoch','loss','val_loss']
    # history_np = np.array(history.history['loss'])
    # history_np = np.c_[np.arange(1, history_np.size+1).astype(np.int64), history_np]
    # history_np = np.c_[history_np, np.array(history.history['val_loss'])]
    headers = ['epoch']
    history_np = np.arange(1, len(history.history[list(history.history.keys())[0]])+1).astype(np.int64)
    for key in history.history.keys():
        #if (key != 'loss') and (key!='val_loss'):
        history_np = np.c_[history_np,np.array(history.history[key])]
        headers.append(key)
    np.savetxt(os.path.join(his_path, file_name+'_'+"training_history.txt"),
               history_np, delimiter=",", header=",".join(headers))

def plot_history(history,his_path,file_name):
    if history is None:
        return
    metrics = [mt for mt in history.history.keys() if ('val' not in mt)]
    ln = len(metrics)
    #plt.figure(figsize=(cm_to_inch(100),cm_to_inch(100))).set_facecolor("w")
    plt.suptitle(file_name + " Training History")
    fontP = FontProperties()
    fontP.set_size('xx-small')
    for n, metric in enumerate(metrics):
        temp_sqrt = int(math.ceil(math.sqrt(ln)))
        plt.subplot(temp_sqrt,temp_sqrt,n+1)
        name = metric.replace("_"," ").capitalize()
        plt.plot(history.epoch, history.history[metric], 
        #color='r', 
        label='Train')
        plt.plot(history.epoch, history.history['val_'+metric],
                    #color='r', 
                    linestyle="--", 
                    label='Val')
        plt.xlabel('Epoch')
        plt.ylabel(name)
        if metric == 'auc':
            plt.ylim([0.8,1])
        if name in ['Tp','Tn','Fp','Fn']:
            plt.yscale("log")
        else:
            plt.yscale("linear")
        plt.tight_layout()
    plt.legend(loc='upper right', bbox_to_anchor=(1.05, 1),mode='expand',prop=fontP,frameon=False)
    plt.savefig(os.path.join(his_path, file_name+'_training_history.png'),dpi=dpi,bbox_inches='tight')
    plt.clf()
    plt.close('all')

####################### tfdf model functions ########################################
def plot_loss_tree(model, his_path,file_name):
    logs = model.make_inspector().training_logs()
    if logs is None:
        return
    plt.figure(figsize=(12, 4))
    plt.title(file_name + " Training History")
    plt.subplot(1, 2, 1)
    plt.plot([log.num_trees for log in logs], [log.evaluation.accuracy for log in logs])#TypeError: 'NoneType' object is not iterable
    plt.xlabel("Number of trees")
    plt.ylabel("Accuracy (out-of-bag)")

    plt.subplot(1, 2, 2)
    plt.plot([log.num_trees for log in logs], [log.evaluation.loss for log in logs])
    plt.xlabel("Number of trees")
    plt.ylabel("Logloss (out-of-bag)")
    plt.savefig(os.path.join(his_path, file_name+'_'+'training_history.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')

def plot_model_tree(model,his_path,file_name):
    #to-do: allow users to choose how many trees are visualized.
    with open(os.path.join(his_path, file_name+'_'+'model.html'), "w") as f: 
        f.write(
            tfdf.model_plotter.plot_model(
                model,
                tree_idx=0,
                max_depth=3
                )
            )

def plot_feature_importance_tfdf(model,his_path,file_name):
    #https://github.com/google/yggdrasil-decision-forests/blob/main/documentation/user_manual.md#variable-importances
    #https://www.tensorflow.org/decision_forests/tutorials/advanced_colab#inspect_the_model_structure
    inspector = model.make_inspector()
    feature_importance_metrics = [importance for importance in inspector.variable_importances().keys() if len(inspector.variable_importances()[importance]) >0]
    ln = len(feature_importance_metrics)
    plt.suptitle(file_name + " Feature Importance")
    fontP = FontProperties()
    fontP.set_size('xx-small')
    for n,importance in enumerate(feature_importance_metrics):
        score_dict = {"Feature":[],"Score":[]} 
        for x,y in inspector.variable_importances()[importance]:
            score_dict["Feature"].append(str(x).replace(' ','').split('"')[1])
            score_dict['Score'].append(float(y))
        df = pd.DataFrame.from_dict(score_dict).sort_values(by='Score',ignore_index=True)

        try:
            temp_sqrt = int(math.ceil(math.sqrt(ln)))
            plt.subplot(temp_sqrt,temp_sqrt,n+1)
            sns.barplot(y='Score', x='Feature',data=df[df['Score']>0])
            plt.title(importance)
            plt.xticks(fontsize=15, rotation=90)
            plt.yticks(fontsize=5)
            plt.tight_layout()
        except Exception as e:
            print(e)
            continue
    plt.legend(loc='upper right', bbox_to_anchor=(1.05, 1),mode='expand',prop=fontP,frameon=False)
    plt.savefig(os.path.join(his_path, file_name+'_feature_importance.png'),dpi=dpi,bbox_inches='tight')
    plt.clf()
    plt.close('all')
########################################################################################

def plot_cm(his_path,file_name,test_labels, pred_labels, dataset,p=0.5):
    cm = confusion_matrix(list(test_labels), pred_labels, labels=np.unique(test_labels))
    class_report = classification_report(list(test_labels), pred_labels, labels=np.unique(test_labels),target_names = dataset.classname)
    with open(os.path.join(his_path, file_name+'_'+'cm_report.txt'),'w') as f:
        f.write(class_report)
    plt.figure(figsize=(5,5))
    sns.heatmap(cm, annot=True, fmt="d",cmap="Blues", linewidths=0.5)
    plt.title('{} Confusion matrix @{:.2f}'.format(file_name,p))
    plt.ylabel('Actual label')
    plt.xlabel('Predicted label')
    plt.xticks(ticks=range(len(dataset.classname)),labels=dataset.classname,fontsize=15, rotation=90)
    plt.savefig(os.path.join(his_path, file_name+'_'+'cm.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')

    print('Legitimate Transactions Detected (True Negatives): ', cm[0][0])
    print('Legitimate Transactions Incorrectly Detected (False Positives): ', cm[0][1])
    print('Fraudulent Transactions Missed (False Negatives): ', cm[1][0])
    print('Fraudulent Transactions Detected (True Positives): ', cm[1][1])
    print('Total Fraudulent Transactions: ', np.sum(cm[1]))

def plot_roc(his_path,file_name,test_labels, pred_labels, dataset, **kwargs):
    pred_labels = tf.one_hot(pred_labels,np.max(test_labels)+1).numpy()
    test_labels = tf.one_hot(test_labels,np.max(test_labels)+1).numpy()
    metric_graph(test_labels, pred_labels, metric='roc', class_names=dataset.classname,
                    title=file_name + ' ROC',
                    figsize=(cm_to_inch(20),cm_to_inch(15)), 
                    filename=os.path.join(his_path, file_name+'_'+'roc.png'))
    plt.clf()
    plt.close('all')

def save_scores(his_path,file_name,test_labels,pred_labels,task):
    if task == REGRESSION_TASK:
        scoring_dic = {'mse': mean_squared_error, 'mae':  mean_absolute_error}
    else:
        scoring_dic = {
                        'balanced_accuracy': balanced_accuracy_score,
                        'f1': f1_score,
                        'precision': precision_score,
                        'recall': recall_score
                        }
    with open(os.path.join(his_path,file_name+'_'+'score.txt'),'w') as f:
        for score_name, score in scoring_dic.items():
            if score_name in ['f1','precision','recall']:
                f.write("{}: {}\n".format(score_name,score(test_labels,pred_labels,average='weighted')))
            else:
                f.write("{}: {}\n".format(score_name,score(test_labels,pred_labels)))

########################################### tabnet model functions ############################
def plot_feature_importance_tabnet(model,his_path,file_name):
    mask = model.tabnet.aggregate_feature_selection_mask.numpy()
    mask = mask.reshape((mask.shape[1],mask.shape[2])) #(2: feature,1: sample)
    mask = np.nan_to_num(mask)
    features = list(model.tabnet._saved_model_inputs_spec.keys())
    np.savetxt(os.path.join(his_path, file_name+'_'+"feature_importance_map.txt"),
               mask, delimiter=",", header=",".join(features))

    df = pd.DataFrame(list(zip(np.average(mask,axis=0),features)),
                   columns=['Rank','Feature']).sort_values(by='Rank',ignore_index=True)
    df.to_csv(os.path.join(his_path,file_name+'_feature_rank.csv'),index=False)

    #plt.figure(figsize=(cm_to_inch(100),cm_to_inch(100))).set_facecolor("w")
    plt.title(file_name + " Feature Importance Map ")
    sns.heatmap(mask,cmap="Blues", linewidths=0.5)
    plt.ylabel("Sample Index")
    plt.xlabel("Features")
    plt.xticks(ticks=range(len(features)),labels=features,fontsize=15, rotation=90)
    plt.yticks(fontsize=5) 
    plt.savefig(os.path.join(his_path,file_name+'_feature_importance_map.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()

    try:
        sns.barplot(y='Rank', x='Feature',data=df[df['Rank']>0.2])
        plt.title(file_name + " Feature Importance Map ")
        plt.xticks(fontsize=15, rotation=90)
        plt.yticks(fontsize=5)
        plt.savefig(os.path.join(his_path,file_name+'_feature_rank.png'),bbox_inches='tight',dpi=dpi)
        plt.clf()
    except Exception as e:
        print(e)
    plt.close('all')
###############################################################################################

def shap_explain(model,dataset,his_path,file_name):
    try:
        model.set_param({"predictor": "gpu_predictor"})
    except:
        pass
    explainer = shap.Explainer(model)
    shap_values = explainer.shap_values(dataset.X)
    #plt.figure(figsize=(cm_to_inch(40),cm_to_inch(25))).set_facecolor("w")
    shap.summary_plot(shap_values, dataset.X)
    plt.title(file_name + " SHAP bar")
    plt.savefig(os.path.join(his_path,file_name+'_SHAPbar.png'),bbox_inches='tight',dpi=dpi)
    plt.clf()
    plt.close('all')














############################################Unused functions#####################################################################
# def draw_graph_history(history, metrics, path, file_name):
#     metrics = [mt for mt in history.history.keys() if ('loss' not in mt and 'val' not in mt)]
#     plt.figure(figsize=(cm_to_inch(100),cm_to_inch(100))).set_facecolor("w")
#     fig, ax1 = plt.subplots()
#     fig.patch.set_facecolor('w')
#     ax1.set_xlabel('epoch')
#     ax1.set_ylabel('loss')
    
#     ax1.autoscale(enable=True, axis='y', tight=True)
#     ax1.plot(history.history['loss'], color='r')
#     ax1.plot(history.history['val_loss'], color='y')
#     ax1.legend(['train_loss', 'val_loss'], loc='upper left')
#     ax1.set_yscale('log')
#     ax2 = ax1.twinx()
    
#     ax2.plot(history.history[metrics[0]], color='b')
#     ax2.plot(history.history['val_'+metrics[0]], color='c')
#     ax2.legend(['train_'+metrics[0], 'val_'+metrics[0]], loc='upper right')
#     ax2.set_ylabel(metrics[0])

#     ax2.autoscale(enable=True, axis='y', tight=True)
#     ax2.set_yscale('log')
#     #plt.yscale("log")
#     plt.title(file_name)
#     plt.savefig(os.path.join(path, file_name+'_hist.png'),bbox_inches='tight',dpi=dpi)

# def plot_metrics(history, METRICS, his_path,file_name):
#     metrics = [mt for mt in history.history.keys() if ('loss' not in mt and 'val' not in mt)]
#     #metrics = ['loss', 'prc', 'precision', 'recall']
#     ln = len(metrics)
#     #plt.figure(figsize=(cm_to_inch(100),cm_to_inch(100))).set_facecolor("w")
#     for n, metric in enumerate(metrics):
#         temp_sqrt = int(math.ceil(math.sqrt(ln)))
#         plt.subplot(temp_sqrt,temp_sqrt,n+1)
#         name = metric.replace("_"," ").capitalize()
#         plt.plot(history.epoch, history.history[metric], 
#         #color='r', 
#         label='Train')
#         plt.plot(history.epoch, history.history['val_'+metric],
#                     #color='r', 
#                     linestyle="--", 
#                     label='Val')
#         plt.xlabel('Epoch')
#         plt.ylabel(name)
#         plt.yscale("log")
#         # if metric == 'loss':
#         #     plt.ylim([0, plt.ylim()[1]])
#         # elif metric == 'auc':
#         #     plt.ylim([0.8,1])
#         # else:
#         #     plt.ylim([0,1])
#         plt.legend()
#     plt.savefig(os.path.join(his_path, file_name+'_metric.png'),bbox_inches='tight',dpi=dpi)
#     plt.clf()


# def plot_prc(name, labels, predictions, **kwargs):
#     #https://stackoverflow.com/questions/66635552/keras-assessing-the-roc-auc-of-multiclass-cnn
#     precision, recall, _ = sklearn.metrics.precision_recall_curve(labels, predictions)

#     plt.plot(precision, recall, label=name, linewidth=2, **kwargs)
#     plt.xlabel('Recall')
#     plt.ylabel('Precision')
#     plt.grid(True)
#     ax = plt.gca()
#     ax.set_aspect('equal')
#     plt.legend(loc='lower right')

# def plot_loss(history, label, n):
#     # Use a log scale on y-axis to show the wide range of values.
#     plt.semilogy(history.epoch, history.history['loss'],
#                 color=colors[n], label='Train ' + label)
#     plt.semilogy(history.epoch, history.history['val_loss'],
#                 color=colors[n], label='Val ' + label,
#                 linestyle="--")
#     plt.xlabel('Epoch')
#     plt.ylabel('Loss')
#     #plt.savefig(os.path.join(his_path, file_name+'_'+metric+'_metric.png'),bbox_inches='tight')
#     plt.clf()


#https://stackoverflow.com/questions/53736948/how-can-i-modify-modelcheckpoint-in-keras-to-monitor-both-val-acc-and-val-loss-a
# best_val_acc = 0
# best_val_loss = sys.float_info.max 
# def saveModel(epoch,logs):
#     val_acc = logs['val_acc']
#     val_loss = logs['val_loss']
#     if val_acc > best_val_acc:
#         best_val_acc = val_acc
#         model.save(...)
#     elif val_acc == best_val_acc:
#         if val_loss < best_val_loss:
#             best_val_loss=val_loss
#             model.save(...)
#callbacks = [LambdaCallback(on_epoch_end=saveModel)]
