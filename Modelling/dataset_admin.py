'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/08/2021
Last Modified: 11/08/2021
Description: 
Reviewed by: ...
'''
import os 
import gc
import re
import subprocess
import numpy as np
import pandas as pd

import tensorflow as tf
import tensorflow_decision_forests as tfdf

from sklearn.model_selection import train_test_split

from global_variables import *

n_skip_column= 1
n_id_columns = 0

class Dataset():
    def __init__(self,opt):
        self.data_path = opt.dataset_path + '/thyroid_cancer.' + opt.kind_data + '.' + opt.version_run + '.csv'

        self.ndim = 0
        self.input_shape = self.dataset_nrows = self.dataset_ncolumns = None
        self.classnum = None
        self.classname = None
        self.dataset = self.train_dataset = self.test_dataset = None
        self.df_train = self.df_test = None
        self.X = self.y = self.X_train = self.X_test = self.y_train = self.y_test =  None
        self.column_names = None

        #self.load_data_into_tf_data(opt)
        self.load_data_into_numpy(opt)
        self.split_np_train_test()
        #self.convert_np_to_tf_data(opt)

    def load_data_into_tf_data(self,opt):
        process = subprocess.Popen(['wc', '-l',self.data_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.dataset_nrows = int(re.sub('[^0-9]+', '', str(process.communicate()[0]).split(' ')[0])) - 1 #-1: skip header rows
        process = subprocess.check_output("head -1 "  +self.data_path, shell=True)
        self.column_names = str(process).strip().replace("b'",'').replace("'",'').split(',')[n_skip_column:] #skip familyID + individualID + phenotype
        self.input_shape = [len(self.column_names)]
        self.classnum=2 #Deal with sparse categorical cross entropy
        self.classname = [str(i) for i in range(self.classnum)]
        self.dataset_ncolumns = len(self.column_names)+n_skip_column #all columns
        # self.dataset = tf.data.experimental.make_csv_dataset(self.data_path,
        #                                                 batch_size = opt.batch_size,
        #                                                 num_rows_for_inference=1,
        #                                                 num_epochs=1,
        #                                                 label_name=target_column_name,
        #                                                 select_columns=list(range(n_id_columns,self.dataset_ncolumns)))

        self.dataset = tf.data.experimental.CsvDataset(self.data_path,
                                                    header=True,
                                                    record_defaults=[tf.int32]*(self.dataset_ncolumns-n_id_columns), #skip familyID + individualID
                                                    select_cols=list(range(n_id_columns,self.dataset_ncolumns))) #skip familyID + individualID
        train_size = int(0.7 * self.dataset_nrows)
        # test_size = int(0.3 * DATASET_SIZE)
        self.train_dataset = self.dataset.take(train_size)
        self.test_dataset = self.dataset.skip(train_size)

        self.train_dataset = self.train_dataset.map(preprocess)
        self.test_dataset = self.test_dataset.map(preprocess)
        self.train_dataset = self.train_dataset.batch(opt.batch_size)
        self.test_dataset = self.test_dataset.batch(opt.batch_size)
        
        self.train_dataset = self.train_dataset.prefetch(tf.data.experimental.AUTOTUNE)
        self.test_dataset = self.test_dataset.prefetch(tf.data.experimental.AUTOTUNE)
        self.dataset_ncolumns = len(self.column_names)
        gc.collect()
    
    def load_data_into_numpy(self,opt):
        process = subprocess.Popen(['wc', '-l',self.data_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.dataset_nrows = int(re.sub('[^0-9]+', '', str(process.communicate()[0]).split(' ')[0])) - 1 #-1: skip header rows
        process = subprocess.check_output("head -1 "  +self.data_path, shell=True)
        self.column_names = str(process).strip().replace("b'",'').replace("'",'').replace("#",'').replace(" ",'').split(',')
        self.input_feature_names = self.column_names[1:]
        self.dataset_ncolumns = len(self.column_names)
        self.input_shape = [len(self.input_feature_names)]
        self.classnum=2 #Deal with sparse categorical cross entropy
        self.classname = [str(i) for i in range(self.classnum)]

        self.dataset = np.empty((self.dataset_nrows,self.dataset_ncolumns)).astype(np.int32,copy=False)
        with open(self.data_path,'r') as f:
            for ind,line in enumerate(f):
                if ind == 0:
                    continue
                self.dataset[ind-1,:] =  line.strip().split(',')
        
        self.X = self.dataset[:,1:]
        self.y = self.dataset[:,0]
        gc.collect()
    
    def split_np_train_test(self):
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, stratify=self.y, test_size=0.3)

    def convert_np_to_tf_data(self,opt):
        if opt.model_name in tree_based_tfdf_models:
            #auto batch_size = 64
            if self.df_train is None:
                #df_train = pd.DataFrame(data=np.concatenate((self.y_train[:,None],self.X_train),axis=1),columns=self.column_names,dtype=np.int32)
                #df_test = pd.DataFrame(data=np.concatenate((self.y_test[:,None],self.X_test),axis=1),columns=self.column_names,dtype=np.int32)
                self.df_train = pd.DataFrame(data=self.X_train,columns=self.column_names[1:],dtype=np.int32)
                self.df_train[self.column_names[0]] = self.y_train
                self.df_test = pd.DataFrame(data=self.X_test,columns=self.column_names[1:],dtype=np.int32)
                self.df_test[self.column_names[0]] = self.y_test
                print("Finished converting df")
            if opt.problem_statement == CLASSIFICATION_TASK:
                train = tfdf.keras.pd_dataframe_to_tf_dataset(self.df_train, label=target_column_name,max_num_classes=self.classnum,task=tfdf.keras.Task.CLASSIFICATION)
                test = tfdf.keras.pd_dataframe_to_tf_dataset(self.df_test, label=target_column_name,max_num_classes=self.classnum,task=tfdf.keras.Task.CLASSIFICATION)
            else:
                train = tfdf.keras.pd_dataframe_to_tf_dataset(df_train, label=target_column_name,task=tfdf.keras.Task.REGRESSION)
                test = tfdf.keras.pd_dataframe_to_tf_dataset(df_test, label=target_column_name,task=tfdf.keras.Task.REGRESSION)
            print("Finished converting tf data")
        else:            
            if opt.model_name in tabnet_models:
                train = tf.data.Dataset.from_tensor_slices((dict(zip(self.input_feature_names,self.X_train.T)),self.y_train))
                test = tf.data.Dataset.from_tensor_slices((dict(zip(self.input_feature_names,self.X_test.T)),self.y_test))
            else:
                train = tf.data.Dataset.from_tensor_slices((self.X_train,self.y_train))
                test = tf.data.Dataset.from_tensor_slices((self.X_test,self.y_test))
            print("Finished converting tf data")
            train = train.batch(opt.batch_size)
            test = test.batch(opt.batch_size)
        
        self.train_dataset = train.prefetch(tf.data.experimental.AUTOTUNE)
        self.test_dataset = test.prefetch(tf.data.experimental.AUTOTUNE)

@tf.function
def preprocess(*fields):
    return tf.stack(fields[1:]), tf.stack(fields[0]) # x, y:phenotype