'''
Author: Anh-Vu Mai-Nguyen
Created Date: 19/06/2021
Last Modified: 19/06/2021
Description: Model factory is the place to create models for training
Reviewed by: ...
Reference: 
'''

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB1, EfficientNetB2, EfficientNetB3, EfficientNetB4, EfficientNetB5, EfficientNetB6, EfficientNetB7, DenseNet121, DenseNet169, DenseNet201, VGG16, VGG19
from tensorflow.keras import Sequential
from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras.layers import RNN, LSTM, GRU, SimpleRNN
#from tensorflow.compat.v1.keras.layers import CuDNNLSTM,CuDNNGRU # comment dong nay neu may ban khong ho tro CuDNN
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Activation, Dense, Dropout,  BatchNormalization, ELU
from tensorflow.keras.layers import TimeDistributed, Embedding, Bidirectional
from tensorflow.keras.layers import Flatten, MaxPooling1D, MaxPooling2D, Conv1D, Conv2D, GlobalAveragePooling2D
#from hyperas.distributions import choice, uniform

import tensorflow_decision_forests as tfdf
from tensorflow_decision_forests.keras import RandomForestModel,GradientBoostedTreesModel,CartModel

import tabnet
import xgboost
import lightgbm as lgb
import statsmodels.api as sm

from global_variables import *

TASK = tfdf.keras.Task.CLASSIFICATION

def Linear_Regression():
    model = Sequential()
    model.add(Flatten(input_shape=INPUT_SHAPE))
    model.add(Dense(INPUT_SHAPE[0]))
    model.add(Dense(CLASS_NUM))
    return model


def Logistic_Regression():
    model = Sequential()
    model.add(Flatten(input_shape=INPUT_SHAPE))
    model.add(Dense(INPUT_SHAPE[0]))
    model.add(Dense(CLASS_NUM, activation='softmax'))
    return model


def MLP_Regression():
    model = Sequential()
    model.add(Flatten(input_shape=INPUT_SHAPE))
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(2048, activation='relu'))
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(512, activation='relu'))
    model.add(Dense(256, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(CLASS_NUM))
    return model

def MLP_Classification():
    model = Sequential()
    model.add(Flatten(input_shape=INPUT_SHAPE))
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(2048, activation='relu'))
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(512, activation='relu'))
    model.add(Dense(256, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(CLASS_NUM,activation='softmax'))
    return model

def Random_Forest():
    model = RandomForestModel(num_trees=1000,task=TASK)
    return model

def Cart():
    model = CartModel(task=TASK)
    return model

def GradientBoosted():
    model = GradientBoostedTreesModel(num_trees=1000,task=TASK)
    return model

def tabnet_classification():
    feature_columns = []
    for col_name in col_names:
        feature_columns.append(tf.feature_column.numeric_column(col_name))
    model = tabnet.TabNetClassifier(feature_columns, num_classes=CLASS_NUM,num_features=INPUT_DIM,
                                feature_dim=CLASS_NUM+2, output_dim=CLASS_NUM+1,
                                num_decision_steps=4, relaxation_factor=1.5,
                                sparsity_coefficient=1e-5, batch_momentum=0.98,
                                virtual_batch_size=None, norm_type='group',
                                num_groups=1)
    return model

def tabnet_regression():
    feature_columns = []
    for col_name in col_names:
        feature_columns.append(tf.feature_column.numeric_column(col_name))
    model = tabnet.TabNetRegression(feature_columns,num_features=INPUT_DIM,
                                feature_dim=CLASS_NUM+2, output_dim=CLASS_NUM+1,
                                num_decision_steps=8, relaxation_factor=1.5,
                                sparsity_coefficient=1e-5, batch_momentum=0.98,
                                virtual_batch_size=None, norm_type='group',
                                num_groups=1,num_regressors=CLASS_NUM)
    return model

def Elastic_Net():
    model = LogisticRegression(penalty= 'elasticnet',solver = 'saga',l1_ratio=0.01)
    return model
    
def lgbmc():
    try:
        model = lgb.LGBMClassifier(device='gpu')#,gpu_platform_id=1,gpu_device_id=2)
    except:
        model = lgb.LGBMClassifier()
    return model

def xgboostclassifier():
    try:
        model = xgboost.XGBClassifier(tree_method='gpu_hist', gpu_id=0)
    except:
        model = xgboost.XGBClassifier()
    return model

def OLS():
    model = sm.OLS
    return model

def create_model(model_name, dataset):
    global INPUT_SHAPE
    global INPUT_DIM
    global CLASS_NUM
    global TASK 
    global model_names
    global col_names
    CLASS_NUM = dataset.classnum
    if CLASS_NUM == 1:
        TASK == tfdf.keras.Task.REGRESSION
    datasetshape = dataset.input_shape
    if len(datasetshape) > 1:
        INPUT_SHAPE = datasetshape
    else:
        INPUT_SHAPE = (datasetshape[0],)
    INPUT_DIM = INPUT_SHAPE[0]
    col_names = dataset.input_feature_names
    model_dic = {'LinearRegression':Linear_Regression,
    'MLPRegression': MLP_Regression,
    'MLPClassification': MLP_Classification,
    'LogisticRegression': Logistic_Regression,
    'RandomForest': Random_Forest,
    'Cart': Cart,
    'GradientBoosted': GradientBoosted,
    'TabnetClassification': tabnet_classification,
    'TabnetRegression': tabnet_regression,
    'lgbmc': lgbmc,
    'xgboostclassifier': xgboostclassifier,
    'statOLS': OLS
    }
    model_names = model_dic.keys()
    if model_name not in model_dic.keys():
        model_name = 'LogisticRegression'
    return model_dic[model_name](), model_name