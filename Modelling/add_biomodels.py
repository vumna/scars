'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ...
'''
import os
import re
import subprocess
import numpy as np
from optparse import OptionParser

PED_FILE_TYPE = 'ped'
CSV_FILE_TYPE = 'csv'

parser = OptionParser()
parser.add_option("--data_folder",type=str, 
                    default='/mnt/Scars_Project/scars_data/clean',
                    help="Folder path contains ped file")
parser.add_option("--input_file",type=str,
                    default='SCARS_VN.Carbamazepine.joint_genotyped.splitted.norm.rename.vcf.recode12.gender.ped',
                    help="ped file")
parser.add_option("--map_file",type=str,
                    default='SCARS_VN.Carbamazepine.joint_genotyped.splitted.norm.rename.vcf.recode12.gender.map',
                    help="map file")
parser.add_option("--file_type",type=str,
                    default=PED_FILE_TYPE,
                    help="file type")
parser.add_option("--b0",type=int, 
                    default=3)
parser.add_option("--b1",type=int, 
                    default=4)
parser.add_option("--b2",type=int, 
                    default=5)
parser.add_option("--a_value",type=int, 
                    default=1)
parser.add_option("--A_value",type=int, 
                    default=2)
parser.add_option("--output_folder",type=str, 
                    default='../data',
                    help="output folder path")
parser.add_option("--output_file_name", type=str,
                    default=None)
parser.add_option("--output_version",type=str,
                    default='v1.0.0')
(options, args) = parser.parse_args()

input_path = os.path.join(options.data_folder, options.input_file)
a_val = options.a_value
A_val = options.A_value

n_models = 2
basic_info_columns = 0
variant_names = []

def read_variant_name():
    global variant_names
    print("Reading variant name")
    fi = open(os.path.join(options.data_folder, options.map_file),'r')
    for line in fi.readlines():
        line = line.strip().split('\t')
        variant_names.append(line[1])

def add_header(header,added_columns,extension_header):
    for i in range(added_columns):
        header.extend([variant_names[i]+'_'+extension_header])
    return header

def read_file():
    global basic_info_columns
    print("Reading")
    if options.file_type == PED_FILE_TYPE:
        process = subprocess.check_output("head -1 "  + input_path +" | sed 's/[^ ]//g' | wc -c", shell=True)
    elif options.file_type == CSV_FILE_TYPE:
        process = subprocess.check_output("head -1 "  + input_path +" | sed 's/[^,]//g' | wc -c", shell=True)
    skip_columns = 4
    data_columns = int(re.sub('[^0-9]+', '', str(process)))-skip_columns
    process = subprocess.Popen(['wc', '-l',input_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    data_rows = int(re.sub('[^0-9]+', '', str(process.communicate()[0]).split(' ')[0]))
    #header = ['Family_ID','Individual_ID','Paternal_ID','Maternal_ID','Gender','Phenotype']
    #header = ['Family_ID','Individual_ID','Phenotype','Maternal_ID','Gender','Paternal_ID']
    header = ['Phenotype','Gender']
    basic_info_columns = len(header)
    data_np = np.empty((data_rows,data_columns+(data_columns-basic_info_columns)//2*n_models)).astype(np.int8)

    ind = -1
    with open(input_path,'r') as f:
        for line in f:
            # line = line.strip().split(' ')[skip_columns:12]
            line = line.strip().split(' ')[skip_columns:]
            if int(line[1]) < 0:
                continue
            temp_el = line[0]
            line[0] = line[1]
            line[1]=temp_el
            ind+=1
            data_np[ind,:data_columns] = line
    data_np.resize((ind+1,data_columns+(data_columns-basic_info_columns)//2*n_models))
    for i in range((data_columns-basic_info_columns)//2):
        header.extend([variant_names[i]+'_1',variant_names[i]+'_2'])
    return data_np,header, data_columns

def additive_model(data_np,header,data_columns):
    '''
    a/a: a*b0+a*b0
    a/A: a*b0+A*b1
    A/A: A*b1+A*b1
    '''
    print("Additive")
    current_columns = len(header)
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0+data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0+A_val*options.b0: a_val*options.b0+A_val*options.b1,
                    A_val*options.b0+A_val*options.b0: 2*A_val*options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'additive')

def multiplication_model(data_np,header,data_columns):
    '''
    a/a: a*b0*a*b0
    a/A: a*bo*A*b1
    A/A: A*b1*A*b1
    '''
    print("Multiply")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*A_val*options.b0: a_val*options.b0*A_val*options.b1,
                    A_val*options.b0*A_val*options.b0: (A_val*options.b1)**2}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'multiplication')

def multiplicative_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0+b1
    A/A: b0+2b1
    '''
    print("multiplicative")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1,
                    A_val*options.b0*A_val*options.b0:options.b0+2*options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'multiplicative')

def genotypic_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0+b1+b2
    A/A: b0+2b1
    '''
    print("genotypic")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1+options.b2,
                    A_val*options.b0*A_val*options.b0:options.b0+2*options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'genotypic')

def dominant_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0+b1
    A/A: b0+b1
    '''
    print("Dominant")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1,
                    A_val*options.b0*A_val*options.b0:options.b0+options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'dominant')

def recessive_model(data_np,header,data_columns):
    '''
    a/a: b0
    a/A: b0
    A/A: b0+b1
    '''
    print("Recessive")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0,
                    a_val*options.b0*A_val*options.b0:options.b0,
                    A_val*options.b0*A_val*options.b0:options.b0+options.b1}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'recessive')

def codominant_model(data_np,header,data_columns):
    '''
    a/a: b0+2b1
    a/A: b0+b1+b2
    A/A: b0+2b2
    '''
    print("codominant")
    current_columns = len(header) 
    added_columns = (data_columns-basic_info_columns)//2
    end_columns = current_columns+added_columns
    data_np[:,current_columns:end_columns] = data_np[:,basic_info_columns:data_columns-1:2]*options.b0*data_np[:,basic_info_columns+1:data_columns:2]*options.b0
    replace_dict = {a_val*options.b0*a_val*options.b0:options.b0+2*options.b1,
                    a_val*options.b0*A_val*options.b0:options.b0+options.b1+options.b2,
                    A_val*options.b0*A_val*options.b0:options.b0+2*options.b2}
    for key,value in replace_dict.items():
        data_np[:,current_columns:end_columns][data_np[:,current_columns:end_columns]==key] = value
    add_header(header,added_columns,'codominant')

def write_file(data_np,header):
    print("Outputting")
    if options.output_file_name is None:
        options.output_file_name = options.input_file.replace('.'+options.file_type,'.'+options.output_version) + '.csv'
    with open(os.path.join(options.output_folder,options.output_file_name),'w') as f:
        np.savetxt(f,data_np,fmt='%s',delimiter=',',header=','.join(header),comments='')

def main():
    read_variant_name()
    model_dict = {'additive': additive_model,
                    'multiplicative': multiplicative_model,
                    'multiplication': multiplication_model,
                    'genotypic':genotypic_model,
                    'dominant':dominant_model,
                    'recessive':recessive_model,
                    'codominant':codominant_model}
    global n_models
    n_models = len(model_dict.keys())
    data_np,header, data_columns = read_file()
    for key in model_dict.keys():
        model_dict[key](data_np,header,data_columns)
    write_file(data_np,header)

if __name__ == "__main__":
    main()