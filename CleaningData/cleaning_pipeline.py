'''
Author: Anh-Vu Mai-Nguyen
Created Date: 11/11/2021
Last Modified: 11/11/2021
Description: 
Reviewed by: ...
'''
import os
import re
import subprocess
import numpy as np
from optparse import OptionParser


parser = OptionParser()
parser.add_option("--data_folder",type=str, 
                    default='/mnt/Scars_Project/scars_data/original_VCF',
                    help="Get folder path")
parser.add_option("--vcf_file",type=str,
                    default='SCARS_VN.Carbamazepine.joint_genotyped.filtered.vcf.gz',
                    help="Get vcf file name")
parser.add_option("--gender_file",type=str,
                    default='/home/vumna/documents/1kGV/scars/scars_hla/Metadata/Estimated_sex_bcftools.tsv',
                    help='')
parser.add_option("--output_folder",type=str, 
                    default='/mnt/Scars_Project/scars_data/clean',
                    help="output folder path")
parser.add_option("--output_file_name", type=str,
                    default=None)
(options, args) = parser.parse_args()

def main():
    os.system('chmod +x -R .')
    vcf_file_name = options.vcf_file.split('/')[-1].replace('.vcf.gz','')
    #normalize vcf file
    print()
    os.system('sh normalize_vcf.sh ' + os.path.join(options.data_folder, options.vcf_file) + ' ' + options.output_folder)
    # using plink recode vcf into plink format files (ped and map)
    os.system('sh recode_vcf.sh ' + os.path.join(options.output_folder, vcf_file_name + '.splitted.norm.vcf.gz') + ' ' + options.gender_file + ' ' + options.output_folder)

if __name__ == "__main__":
    main()