'''
Author: Anh-Vu Mai-Nguyen
Created Date: 10/11/2021
Last Modified: 10/11/2021
Description: 
Reviewed by: ...
Example usage comand: python3 get_phenotype_from_name.py --input_file /mnt/Scars_Project/scars_data/clean/SCARS_VN.Carbamazepine.joint_genotyped.sample_name.txt --drug_type "Carbamazepine"
'''

import os
import numpy as np
import pandas as pd
from optparse import OptionParser

Allopurinol = 'Allopurinol'
Carbamazepine = 'Carbamazepine'

parser = OptionParser()
parser.add_option("--data_folder",type=str, 
                    default='/mnt/Scars_Project/scars_data/clean',
                    help="Get folder path")
parser.add_option("--input_file",type=str,
                    default='SCARS_VN.Allopurinol.joint_genotyped.sample_name.txt')
parser.add_option("--drug_type",type=str,
                    default=Allopurinol)                    
parser.add_option("--output_folder",type=str, 
                    default='/mnt/Scars_Project/scars_data/clean',
                    help="output folder path")
parser.add_option("--output_file_name", type=str,
                    default=None)
parser.add_option("--output_version",type=str,
                    default='phenotype')
(options, args) = parser.parse_args()

txt_file = os.path.join(options.data_folder, options.input_file)

def read_file():
    print("Reading file")
    data_np = np.loadtxt(txt_file,dtype=str,delimiter=' ')
    #expand the data_np to shape (n,3)
    data_np = np.expand_dims(data_np,1)
    data_np = np.insert(data_np,1,values=0,axis=1)
    data_np = np.insert(data_np,1,values=0,axis=1)
    #assign values of first column to the second column of data_np
    data_np[:,1] = data_np[:,0]
    return data_np

# get phenotype from name of sample by check if the name contains 'SAP' then add 1 to the third column of the data, else add 0 to the third column in numpy array
def get_phenotype(data_np):
    print("Getting phenotype")
    #SAP means patient and is decoded by 1
    #SAC means control and is decoed by 0
    if options.drug_type == Allopurinol:
        data_np[:,2] = np.where(pd.Series(data_np[:,0]).str.contains('SAP'),1,0)
    elif options.drug_type == Carbamazepine:
        data_np[:,2] = np.where(pd.Series(data_np[:,0]).str.contains('SCP'),1,0)
    return data_np

def write_file(data_np):
    print("Outputting")
    if options.output_file_name is None:
        options.output_file_name = options.input_file.replace('txt',options.output_version) + '.txt'
    with open(os.path.join(options.output_folder,options.output_file_name),'w') as f:
        np.savetxt(f,data_np,fmt='%s',delimiter=' ')

def main():
    data_np = read_file()
    data_np = get_phenotype(data_np)
    write_file(data_np)

if __name__ == "__main__":
    main()