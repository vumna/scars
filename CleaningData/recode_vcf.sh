#example usage: ~/documents/1kGV/scars/scars_master/CleaningData/recode_vcf.sh \ 
#               SCARS_VN.Allopurinol.joint_genotyped.splitted.norm.rename.vcf.gz \
#               /home/vumna/documents/1kGV/scars/scars_hla/Metadata/Estimated_sex_bcftools.tsv \
#               .

#recode 12 for vcf with plink
vcf_file=$1
sex_file=$2
output_folder=$3

filename="${$(basename $vcf_file)%.*}"
echo $filename
#https://www.biostars.org/p/9484948/
#It converts phenotype:
#   Patient to 2 and Control to 1 because POS=1 and NEG=0

#make pheno file
if ($sample_name ~ "Allopurinol"); then
    bcftools query -l $vcf_file | awk '{if($1 ~ "SAP") print $1" "$1" "1; else print $1" "$1" "0}' > $output_folder/$filename.phenotype.txt
else
    bcftools query -l $vcf_file | awk '{if($1 ~ "SCP") print $1" "$1" "1; else print $1" "$1" "0}' > $output_folder/$filename.phenotype.txt
fi

#make sex file
# --update-sex expects a file with FIDs and IIDs in the first two columns, and sex information (1 or M = male, 2 or F = female, 0 = missing) in the (n+2)th column. If no second parameter is provided, n defaults to 1. It is frequently useful to set n=3, since sex defaults to the 5th column in .ped and .fam files.
# cat $sex_file | sed -r 's/\_//g' | awk '{ print $1"  "$1"  "$2 }' > $output_folder/gender_samples.txt
cat $sex_file | awk '{ print $1"  "$1"  "$2 }' > $output_folder/gender_samples.txt
export PATH=$PATH:/home/vumna/bin
plink --double-id --vcf-half-call 'r' --make-pheno $output_folder/$filename.phenotype.txt 1 --update-sex $output_folder/gender_samples.txt --recode 12 --vcf $vcf_file --out $output_folder/$filename.recode12.gender >> $output_folder/$filename.log