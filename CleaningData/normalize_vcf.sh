#example usage:  ~/documents/1kGV/scars/scars_master/CleaningData/normalize_vcf.sh SCARS_VN.Carbamazepine.joint_genotyped.vcf .

#https://qastack.vn/programming/16483119/an-example-of-how-to-use-getopts-in-bash
# to-do: https://qastack.vn/programming/402377/using-getopts-to-process-long-and-short-command-line-options
usage="$(basename "$0") [-h] [--vcf] [--reference] -- program to normalize vcf files to plink compatiable format

where:
    -h  show this help text"

while getopts ':h:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    s) seed=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

export PATH=$PATH:/home/vumna/bin
vcf_file=$1
output_folder=$2
filename="${$(basename $vcf_file)%.*}"
#Normalize with reference file:
# - Normalized ref and alt with reference file
# - split multi allelic
# - remove duplicated variants
bcftools norm --multiallelics -both $vcf_file -Oz -o $output_folder/$filename.splitted.vcf.gz > $output_folder/$filename.log
bcftools norm --rm-dup "exact" $output_folder/$filename.splitted.vcf.gz -Oz -o $output_folder/$filename.splitted.norm.vcf.gz >> $output_folder/$filename.log
#rename samples compatiable with plink
# bcftools query -l $output_folder/$filename.splitted.norm.vcf.gz | sed -r 's/\_//g' > $output_folder/$filename.sample_name.txt
# bcftools reheader -s $output_folder/$filename.sample_name.txt $output_folder/$filename.splitted.norm.vcf.gz -o $output_folder/$filename.splitted.norm.rename.vcf.gz >> $output_folder/$filename.log